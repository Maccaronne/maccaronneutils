package com.gmail.maccaronne;

import java.io.*;
import java.nio.charset.Charset;
import java.util.logging.Level;

import com.google.common.base.Charsets;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import com.google.common.io.Files;
import org.yaml.snakeyaml.reader.ReaderException;

public class GBKFileConfiguration extends YamlConfiguration {

    public static Charset GBK_CHARSET = Charset.forName("GB2312");

    public void loadDeprecatedFile(File file) throws IOException, InvalidConfigurationException {
        InputStreamReader reader = new InputStreamReader(new FileInputStream(file), GBK_CHARSET);
        StringBuilder builder = new StringBuilder();
        BufferedReader input = new BufferedReader(reader);

        try {
            String line;

            while ((line = input.readLine()) != null) {
                builder.append(line);
                builder.append('\n');
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            input.close();
        }

        loadFromString(builder.toString());
    }

    public void save() {
        try {
            save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(File file) throws IOException {
        if (MaccaronneUtils.version >= 190) {
            super.save(file);
            return;
        }
        Validate.notNull(file, "File cannot be null");
        try {
            Files.createParentDirs(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String data = saveToString();
        data = data.replace("\\xa7", "��");
        data = data.replace("\\xb7", "��");
        data = data.replace("\\\"", "");
        data = data.replace("\\ ", "");
        data = data.replace("\\\n        ", "");
        data = data.replace("\\\n      ", "");
        data = data.replace("\\\n    ", "");
        data = data.replace("\\\n  ", "");
        data = StringEscapeUtils.unescapeJava(data);
//        data = data.replace("\"\\\n", "\\\n");
//        data = data.replace("- \"", "- ");

        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            stream.write(data.getBytes(GBK_CHARSET));
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    File f;

    public static YamlConfiguration loadConfiguration(File file, boolean forceGBK) {
        Validate.notNull(file, "File cannot be null");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        GBKFileConfiguration config = new GBKFileConfiguration();
        config.f = file;

        try {
            if (!forceGBK) {
                config.load(file);
            } else {
                config.loadDeprecatedFile(file);
            }
        } catch (InvalidConfigurationException | ReaderException ex) {
            FileInputStream stream = null;
            try {
                config.loadDeprecatedFile(file);
            } catch (FileNotFoundException | InvalidConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException var3) {
            var3.printStackTrace();
        } catch (IOException var4) {
            Bukkit.getLogger().log(Level.SEVERE, "Cannot load " + file, var4);
        }

        return config;
    }

    public static YamlConfiguration loadConfiguration(File file) {
        return loadConfiguration(file, false);
    }
}
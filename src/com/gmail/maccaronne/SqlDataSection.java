package com.gmail.maccaronne;

import com.mysql.jdbc.Connection;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ΢ЦЦЦ on 2016/10/7.
 */
public class SqlDataSection extends YamlConfiguration {

    String playerName;

    public SqlDataSection(String playerName) {
        this.playerName = playerName;
        getFile(playerName);
    }

    public void save() {
        updateFile(playerName, saveToString());
    }

    protected static Connection connection;
    private static String database, username, password;

    public static void bootSqlConnection() {
        try {
            YamlConfiguration databaseFile = new YamlConfiguration();
            databaseFile.load(MaccaronneUtils.getInstance().getResource("Database.yml"));
            database = databaseFile.getString("Mysql.database");
            username = databaseFile.getString("Mysql.username");
            password = databaseFile.getString("Mysql.password");
            createTable();
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static synchronized void createTable() {
        if (openConnection()) {
            try {
                PreparedStatement createTable = connection.prepareStatement(
                        "CREATE TABLE if not exists playerdata (" +
                                "name VARCHAR(20), " +
                                "file MEDIUMTEXT" +
                                ");");
                createTable.execute();
                createTable.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                closeConnection();
            }
        }
    }

    public static synchronized boolean openConnection() {
        if (database == null) bootSqlConnection();
        try {
            if (connection == null || connection.isClosed()) {
                connection = (Connection) DriverManager.getConnection(database, username, password);
                return true;
            }
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static synchronized void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getFile(String directory) {
        if (openConnection()) {
            try {
                if (playerDataContainplayer(directory)) {
                    PreparedStatement sql = connection.prepareStatement("SELECT file FROM playerdata WHERE name=?;");
                    sql.setString(1, directory);

                    ResultSet result = sql.executeQuery();
                    if (result.next()) {
                        String rank = result.getString("file");
                        sql.close();
                        result.close();
                        loadFromString(rank);
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                closeConnection();
            }
        }
    }

    public static synchronized boolean updateFile(String directory, String file) {
        if (openConnection()) {
            try {
                if (playerDataContainplayer(directory)) {
                    PreparedStatement loginsUpdate = connection.prepareStatement("UPDATE `playerdata` SET file=? WHERE name=?;");
                    loginsUpdate.setString(1, file);
                    loginsUpdate.setString(2, directory);
                    loginsUpdate.executeUpdate();
                    loginsUpdate.close();
                } else {
                    insertNewFile(directory, file);
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                closeConnection();
            }
        }
        return false;
    }

    private static synchronized void insertNewFile(String directory, String file) {
        PreparedStatement newplayer = null;
        try {
            newplayer = connection.prepareStatement("INSERT INTO playerdata (name, file) values(?,?);");
            newplayer.setString(1, directory);
            newplayer.setString(2, file);
            newplayer.execute();
            newplayer.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized static boolean playerDataContainplayer(String directory) {
        try {
            PreparedStatement sql = connection.prepareStatement("SELECT * FROM playerdata WHERE name=?;");
            sql.setString(1, directory);
            ResultSet resultSet = sql.executeQuery();
            boolean containsplayer = resultSet.next();

            sql.close();
            resultSet.close();

            return containsplayer;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public ConfigurationSection getSection(String name) {
        ConfigurationSection section = getConfigurationSection(name);
        if (section == null) section = createSection(name);
        return section;
    }

    public static boolean exists(String playerName) {
        if (openConnection()) {
            boolean containplayer = playerDataContainplayer(playerName);
            closeConnection();
            return containplayer;
        }
        return false;
    }
}

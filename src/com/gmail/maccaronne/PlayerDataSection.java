package com.gmail.maccaronne;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Maccaronne on 2016/2/25.
 * Vos autem Dominus benedic hoc signum
 */
public class PlayerDataSection {

    private final YamlConfiguration fileConfig;
    private File savedLocation;
    private ConfigurationSection wholeSection;
    private String playerName;

    public static boolean doExists(String name) {
        return new File(MaccaronneUtils.getInstance().getDataFolder(), "/" + name + ".yml").exists();
    }

    public String getPlayerName() {
        return playerName;
    }

    public ConfigurationSection getWholeSection() {
        return wholeSection;
    }

    public PlayerDataSection(String player, String rootKey) {
        this.playerName = player;
        savedLocation = new File(MaccaronneUtils.getInstance().getDataFolder(), "/" + player + ".yml");
        if (!savedLocation.exists()) {
            try {
                savedLocation.createNewFile();
            } catch (IOException e) {
            }
        }
        fileConfig = GBKFileConfiguration.loadConfiguration(savedLocation);
        if (!fileConfig.contains(rootKey)) {
            wholeSection = fileConfig.createSection(rootKey);
        } else {
            wholeSection = fileConfig.getConfigurationSection(rootKey);
        }
    }

    public String getString(String key) {
        return wholeSection.getString(key);
    }

    public boolean getBoolean(String key) {
        return wholeSection.getBoolean(key);
    }

    public int getInt(String key) {
        return wholeSection.getInt(key);
    }

    public double getDouble(String key) {
        return wholeSection.getDouble(key);
    }

    public List<String> getStringList(String key) {
        return wholeSection.getStringList(key);
    }

    public void save() {
        try {
            fileConfig.save(savedLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void set(String route, Object object) {
        if (object instanceof Location) {
            wholeSection.set(route, MaccaronneUtils.toString((Location) object));
        } else {
            wholeSection.set(route, object);
        }
    }

    public Location getLocation(String key) {
        if (wholeSection.contains(key)) {
            return MaccaronneUtils.getLocation(wholeSection.getString(key));
        }
        return null;
    }

    public double getDouble(String key, double i) {
        if (wholeSection.contains(key)) {
            return wholeSection.getDouble(key);
        }
        return i;
    }

    public int getInt(String key, int i) {
        if (wholeSection.contains(key)) {
            return wholeSection.getInt(key);
        }
        return i;
    }

    public void remove() {
        fileConfig.set(wholeSection.getName(), null);
    }
}

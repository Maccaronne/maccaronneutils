package com.gmail.maccaronne;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by ΢ЦЦЦ on 2016/6/3.
 */
public class MaccaronneCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender s, Command command, String str, String[] args) {
        if (!s.isOp()) {
            return true;
        }
        switch (args[0]) {
            case ("set"):
                setAll(s, args);
                break;
            case ("transcode"):
                transcode(s, args);
                break;
            default:
                modifyAll(s, args);
                break;
        }
        return true;
    }

    private void transcode(CommandSender s, String[] args) {
        File file = new File(MaccaronneUtils.getInstance().getDataFolder(), "Transcode");
        transcodeFolder(file);
    }

    public void transcodeFolder(File file) {
        for (File playerFile : file.listFiles()) {
            if (playerFile.isDirectory()) transcodeFolder(playerFile);
            if (playerFile.getName().endsWith("yml")) {
                GBKFileConfiguration config = (GBKFileConfiguration) GBKFileConfiguration.loadConfiguration(playerFile, true);
                config.save();
            }
        }
    }

    private void modifyAll(CommandSender s, String[] args) {
        String key = args[0];
        Bukkit.getScheduler().runTaskAsynchronously(MaccaronneUtils.getInstance(), () -> {
            File file = MaccaronneUtils.getInstance().getDataFolder();
            for (File playerFile : file.listFiles()) {
                YamlConfiguration config = GBKFileConfiguration.loadConfiguration(playerFile);
                if (!config.contains(key)) continue;
                double before = config.getDouble(key);
                double after = before;
                String expression = args[1];
                if (expression != null) {
                    char manipulator = expression.charAt(0);
                    float value = Float.parseFloat(expression.substring(1, expression.length()));
                    switch (manipulator) {
                        case '*':
                            after = before * value;
                            break;
                        case '/':
                            after = before / value;
                            break;
                        case '-':
                            after = before - value;
                            break;
                        case '^':
                            after = Math.pow(before, value);
                            break;
                        case '+':
                        default:
                            after = before + value;
                            break;
                    }
                    config.set(key, after);
                    try {
                        config.save(playerFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setAll(CommandSender s, String[] args) {
        Bukkit.getScheduler().runTaskAsynchronously(MaccaronneUtils.getInstance(), () -> {
            String key = args[1];
            String value = null;
            if (args.length >= 3) {
                value = args[2];
            }
            File file = MaccaronneUtils.getInstance().getDataFolder();
            for (File playerFile : file.listFiles()) {
                if (playerFile.isDirectory()) continue;
                YamlConfiguration data = GBKFileConfiguration.loadConfiguration(playerFile);
                if (!data.contains(key)) continue;
                data.set(key, value);
                try {
                    data.save(playerFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Operation collaboration complete!");
        });
    }


}

package com.gmail.maccaronne.jsonutils;

import org.bukkit.ChatColor;

/**
 * An enum representing a Color that is accepted in the JSON message format.
 * All rights reserved.
 * @author ColoredCarrot
 * @see {@link JSONComponent#setColor(JSONColor)}, {@link JSONComponent#getColor()}
 */
public enum JSONColor
{

	AQUA,
	BLACK,
	BLUE,
	DARK_AQUA,
	DARK_BLUE,
	DARK_GRAY,
	DARK_GREEN,
	DARK_PURPLE,
	DARK_RED,
	GOLD,
	GRAY,
	GREEN,
	LIGHT_PURPLE,
	RED,
	WHITE,
	YELLOW;
	
	/**
	 * Transforms super.toString() into lowercase letters which are accepted by the JSON message format.
	 */
	@Override
	public String toString()
	{
		return super.toString().toLowerCase();
	}
	
	public ChatColor toChatColor()
	{
		return ChatColor.valueOf(name());
	}
	
}

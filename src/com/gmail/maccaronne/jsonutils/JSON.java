package com.gmail.maccaronne.jsonutils;

import java.util.ArrayList;
import java.util.List;

import com.gmail.maccaronne.jsonutils.nms.NMS;
import com.gmail.maccaronne.jsonutils.nms.NMSNotHookedException;
import com.gmail.maccaronne.jsonutils.nms.NMSSetupResponse;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.Plugin;


/**
 * A JSON Object is the base for any JSON message using this library.
 * It contains one or more JSONComponentSimple Objects used to make up the whole JSON message.
 * All rights reserved,
 * @author ColoredCarrot
 * @see JSONComponentSimple
 */
public class JSON
{
	
	/**
	 * This method needs to be called in the onEnable() method.
	 * @see NMS#setup()
	 */
	public static NMSSetupResponse setup(Plugin pl)
	{
		return NMS.setup(pl);
	}
	
	/**
	 * Sends a JSON message to a player.
	 * @param to (Player) - the player to send the message to
	 * @param json (String) - the raw JSON to send
	 * @return (boolean) - whether the message could be sent.
	 */
	public static boolean sendJSON(Player to, String json)
	{
		
		if (to == null) throw new IllegalArgumentException("player cannot be null!");
		if (json == null) throw new IllegalArgumentException("json cannot be null!");
		
		try { NMS.getHook().sendJSON(json, to); }
		catch (NMSNotHookedException e) { return false; }
		
		return true;
		
	}
	
	/**
	 * Sends a JSON Object to a player.
	 * This method is in no way different from sendJSON(Player, String); in fact, it actually calls it.
	 * @param to (Player) - the player to send the message to
	 * @param json (JSON) - the JSON Object to send
	 */
	public static void sendJSON(Player to, JSON json)
	{
		sendJSON(to, json.get());
	}
	
	/**
	 * Updates the text of a sign.
	 * @param sign (Sign) - the sign to update
	 * @param line1 (JSON) - the first line, may be null
	 * @param line2 (JSON) - the second line, may be null
	 * @param line3 (JSON) - the third line, may be null
	 * @param line4 (JSON) - the fourth line, may be null
	 */
	public static void updateSign(Sign sign, JSON line1, JSON line2, JSON line3, JSON line4)
	{
		
		if (sign == null)
			throw new NullPointerException("sign cannot be null!");
		
		sign.setLine(0, line1 == null ? "" : line1.get());
		sign.setLine(1, line2 == null ? "" : line2.get());
		sign.setLine(1, line3 == null ? "" : line3.get());
		sign.setLine(1, line4 == null ? "" : line4.get());
		
	}
	
	/**
	 * Updates a specific line of a sign.
	 * @param sign (Sign) - the sign to update
	 * @param line (int) - the line (ranges from 0-3)
	 * @param json (JSON) - the text
	 */
	public static void updateSign(Sign sign, int line, JSON json)
	{
		
		if (line < 0 || line > 3)
			throw new IllegalArgumentException("line must be between 0 and 3!");
		
		if (sign == null)
			throw new NullPointerException("sign cannot be null!");
		
		sign.setLine(line, json.get());
		
	}
	
	/**
	 * Combines multiple JSON Objects into a single one, using .clone() on all components.
	 * @param jsons (JSON[]) - the JSON Objects to combine
	 * @return (JSON) - the combined JSON.
	 */
	public static JSON combineToNewJSON(JSON... jsons)
	{
		
		JSON baseJSON;
		
		baseJSON = jsons[0].clone();
		
		for (int i = 1; i < jsons.length; i++)
			for (JSONComponentSimple comp : jsons[i].getComponents())
				baseJSON.add(comp.clone());
		
		return baseJSON;
		
	}
	
	/**
	 * Sends a title and/or subtitle to a specified player.
	 * @param player (Player) - the player receiving the title/subtitle
	 * @param title (JSON) - the title, may be null to send no title
	 * @param subtitle (JSON) - the subtitle, may be null to send no subtitle
	 * @param fadeIn (Integer) - over how much time the title should fade in, may be null if title is null
	 * @param stay (Integer) - how much time the title should stay, may be null if title is null
	 * @param fadeOut (Integer) - over how much time the title should fade out, may be null if title is null
	 * @return (boolean) - whether the title could be sent.
	 */
	public static boolean sendTitle(Player player, JSON title, JSON subtitle, Integer fadeIn, Integer stay, Integer fadeOut)
	{
		
		try { NMS.getHook().sendTitle(player, title, subtitle, fadeIn, stay, fadeOut); }
		catch (NMSNotHookedException e) { return false; }
		
		return true;
		
	}
	
	/**
	 * Sends a title to a player.
	 * @param player (Player) - the player receiving the title
	 * @param json (JSON) - the title
	 * @param fadeIn (int) - over how much time the title should fade in
	 * @param stay (int) - how much time the title should stay
	 * @param fadeOut (int) - over how much time the title should fade out
	 * @return (boolean) - whether the title could be sent.
	 * @see {@link #sendTitle(Player, JSON, JSON, Integer, Integer, Integer)}, {@link #sendSubtitle(Player, JSON)}
	 */
	public static boolean sendTitle(Player player, JSON json, int fadeIn, int stay, int fadeOut)
	{
		
		try { NMS.getHook().sendTitle(player, json, fadeIn, stay, fadeOut); }
		catch (NMSNotHookedException e) { return false; }
		
		return true;
		
	}
	
	/**
	 * Sends a subtitle to a player.
	 * @param player (Player) - the player receiving the title
	 * @param json (JSON) - the title
	 * @return (boolean) - whether the subtitle could be sent.
	 * @see {@link #sendTitle(Player, JSON, JSON, Integer, Integer, Integer)}, {@link #sendTitle(Player, JSON, int, int, int)}
	 */
	public static boolean sendSubtitle(Player player, JSON json)
	{
		
		try { NMS.getHook().sendSubtitle(player, json); }
		catch (NMSNotHookedException e) { return false; }
		
		return true;
		
	}
	
	/**
	 * Generates a book, each page containing exactly one of the specified JSON elements
	 * @param title (String) - the book title
	 * @param author (String) - the book author
	 * @param jsons (JSON[]) - all jsons. each will be put into one page
	 * @return (ItemStack) - the generated book.
	 */
	public static ItemStack generateBook(String title, String author, JSON... jsons)
	{
		
		ItemStack item = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta meta = (BookMeta) item.getItemMeta();
		
		meta.setTitle(title);
		meta.setAuthor(author);
		
		for (int i = 0; i < jsons.length; i++)
			meta.setPage(i, jsons[i].get());
		
		item.setItemMeta(meta);
		
		return item;
		
	}

	private List<JSONComponentSimple> components = new ArrayList<JSONComponentSimple>();
	private String generatedJSON;
	private boolean generated;

	/**
	 * Constructs a new JSON Object with a base JSONComponentSimple Object.
	 */
	public JSON(JSONComponentSimple jsonComponent)
	{
		
		if (jsonComponent == null) throw new IllegalArgumentException("component cannot be null!");
		
		components.add(jsonComponent);
		
		generatedJSON = "";
		generated = false;
		
	}
	
	/**
	 * Constructs a new JSON Object with already containing multiple JSONComponentSimple Objects.
	 * @param components (JSONComponentSimple[]) - the JSONComponentSimple Objects
	 */
	public JSON(JSONComponentSimple... components)
	{
		
		if (components == null) throw new IllegalArgumentException("component cannot be null!");
		
		if (components.length > 0)
			for (JSONComponentSimple component : components)
				this.components.add(component);
		
		generatedJSON = "";
		generated = false;
		
	}
	
	/**
	 * returns a new JSON Object, invoking .clone() on all JSONComponentSimple Objects in this JSON Object.
	 */
	@Override
	public JSON clone()
	{
		
		JSONComponentSimple[] comps = new JSONComponentSimple[components.size()];
		
		for (int i = 0; i < components.size(); i++)
			comps[i] = components.get(i).clone();
		
		return new JSON(comps);
		
	}
	
	/**
	 * Adds a JSONComponentSimple to this JSON Object.
	 * @param component (JSONComponentSimple) - the JSONComponentSimple to add.
	 * @return (JSON) - this JSON Object, for chaining.
	 */
	public JSON add(JSONComponentSimple component)
	{
		
		if (component == null) throw new IllegalArgumentException("component cannot be null!");
		
		components.add(component);
		
		generated = false;
		
		return this;
		
	}
	
	/**
	 * Removes a JSONComponentSimple from this JSON Object.
	 * @param component (JSONComponentSimple) - the JSONComponentSimple to remove.
	 * @return (JSON) - this JSON Object, for chaining.
	 */
	public JSON remove(JSONComponentSimple component)
	{
		
		if (component == null) throw new IllegalArgumentException("component cannot be null!");
		
		components.remove(component);
		
		generated = false;
		
		return this;
		
	}
	
	/**
	 * Gets all the JSONComponentSimple Objects included in this JSON Object.
	 * @return (List: JSONComponentSimple) - all JSONComponentSimple Objects.
	 */
	public List<JSONComponentSimple> getComponents()
	{
		return components;
	}
	
	/**
	 * Combines this JSON Object with other JSON Objects.
	 * Be careful; this doesn't invoke .clone() !
	 * @param jsons (JSON[]) - the JSON Objects to add to this one
	 * @return JSON - this JSON Object, with all other JSON Objects added.
	 */
	public JSON combine(JSON...jsons)
	{
		
		for (JSON json : jsons)
			for (JSONComponentSimple component : json.getComponents())
				this.add(component);
		
		return this;
		
	}
	
	/**
	 * Generates the JSON String.
	 * You should have no need to use this method as it's automatically called on get() and send(Player).
	 * @return (JSON) - this JSON Object, for chaining.
	 */
	public JSON generate()
	{
		
		generatedJSON = "{\"text\":\"\",\"extra\":[";
		
		for (JSONComponentSimple component : components)
		{
			generatedJSON += component.get() + ",";
		}
		
		generatedJSON = generatedJSON.substring(0, generatedJSON.length() - 1) + "]}";
		
		generated = true;
		
		return this;
		
	}
	
	/**
	 * Generates and then returns the raw JSON message.
	 * @return (String) - the raw JSON matching this JSON Object.
	 */
	public String get()
	{
		
		if (!generated) generate();
		
		return generatedJSON;
		
	}
	
	/**
	 * Generates and then sends the raw JSON matching this JSON Object to a player.
	 * @param player (Player) - the player to send the message to
	 * @return (JSON) - this JSON Object, for chaining.
	 */
	public JSON send(Player player)
	{
		
		if (player == null) throw new IllegalArgumentException("player cannot be null!");
		
		sendJSON(player, get());
		
		return this;
		
	}
	
	/**
	 * Updates a specific line of a sign to this JSON Object.
	 * @param sign (Sign) - the sign to update
	 * @param line (int) - the line
	 * @return (JSON) - this JSON Object, for chaining.
	 */
	public JSON updateSign(Sign sign, int line)
	{
		
		updateSign(sign, line, this);
		
		return this;
		
	}
	
	/**
	 * Sends this as a title to a player.
	 * @param player (Player) - the player receiving the title
	 * @param fadeIn (int) - over how much time the title should fade in
	 * @param stay (int) - how much time the title should stay
	 * @param fadeOut (int) - over how much time the title should fade out
	 * @return (JSON) - this JSON Object, for chaining.
	 * @see {@link #sendTitle(Player, JSON, int, int, int)}
	 */
	public JSON sendTitle(Player player, int fadeIn, int stay, int fadeOut)
	{
		
		sendTitle(player, this, fadeIn, stay, fadeOut);
		
		return this;
		
	}
	
	/**
	 * Sends this as a subtitle to a player.
	 * @param player (Player) - the player receiving the subtitle
	 * @return (JSON) - this JSON Object, for chaining.
	 * @see {@link #sendSubtitle(Player, JSON)}
	 */
	public JSON sendSubtitle(Player player)
	{
		
		sendSubtitle(player, this);
		
		return this;
		
	}
	
	/**
	 * Returns a non-JSON version of this JSON Object. Does not contain hover- or click actions.
	 * @return (String) - the ChatColor version
	 * @see JSONComponentSimple#getChatColorVersion()
	 */
	public String getChatColorVersion()
	{
		
		String s = "";
		
		for (JSONComponentSimple comp : components)
			s += ChatColor.RESET + comp.getChatColorVersion();
		
		return s;
		
	}
	
}

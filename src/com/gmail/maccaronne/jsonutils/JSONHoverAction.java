package com.gmail.maccaronne.jsonutils;

import org.bukkit.Material;

/**
 * Represents a hover action.
 * All rights reserved.
 * @author ColoredCarrot
 * @param <T> - the Type of the value associated with the class implementing this interface
 * @see {@link #getValue()}, {@link #setValue(Object)}
 */
public interface JSONHoverAction<T>
{
	
	/**
	 * Gets the value associated with this JSONHoverAction.
	 * @return (?) - the value.
	 * @see #setValue(Object)
	 */
	T getValue();
	
	/**
	 * Sets the value of this JSONHoverAction.
	 * @param newValue (?) - the new value
	 * @return (JSONHoverAction: ?) - this JSONHoverAction Object, for chaining.
	 */
	JSONHoverAction<T> setValue(T newValue);
	
	/**
	 * Gets the value associated with this JSONHoverAction transformed to a String.
	 * @return (String) - the value as a String.
	 * @see #getValue()
	 */
	String getValueString();
	
	/**
	 * Gets the action name associated with this JSONHoverAction.
	 * Example: "show_text"
	 * @return (String) - the action name.
	 */
	String getActionName();
	
	/**
	 * Shows some JSON-formed text when hovering over the text in the chat.
	 */
	class ShowText
	implements JSONHoverAction<JSON>
	{

		/**
		 * The action name
		 * @see #getActionName()
		 */
		public static final String NAME = "show_text";
		
		private JSON value;
		
		/**
		 * Constructs a {@link ShowText}
		 * @param value (JSON) - the value associated with this JSONHoverAction
		 */
		public ShowText(JSON value)
		{
			
			this.value = value;
			
		}

		@Override
		public JSON getValue() {
			return value;
		}

		@Override
		public JSONHoverAction<JSON> setValue(JSON newValue)
		{
			value = newValue;
			return this;
		}

		@Override
		public String getValueString() {
			return value.get();
		}

		@Override
		public String getActionName() {
			return NAME;
		}
		
	}
	
	/**
	 * Shows an item when hovering over the text in the chat.
	 */
	class ShowItem
	implements JSONHoverAction<Material>
	{

		/**
		 * The action name
		 * @see #getActionName()
		 */
		public static final String NAME = "show_item";
		
		private Material value;
		
		/**
		 * Constructs a {@link ShowItem}
		 * @param value (JSON) - the value associated with this JSONHoverAction
		 */
		public ShowItem(Material value)
		{
			
			this.value = value;
			
		}

		@Override
		public Material getValue() {
			return value;
		}

		@Override
		public JSONHoverAction<Material> setValue(Material newValue)
		{
			value = newValue;
			return this;
		}

		@Override
		public String getValueString() {
			return value.toString().toLowerCase();
		}

		@Override
		public String getActionName() {
			return NAME;
		}
		
	}
	
}

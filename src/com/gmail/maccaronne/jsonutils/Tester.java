package com.gmail.maccaronne.jsonutils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Tester
{
	
	public static void sendTestMessageToAllPlayers(JavaPlugin plugin)
	{
		
		JSON json = new JSON(new JSONComponent("Hello, ")
				.setColor(JSONColor.AQUA).setBold(true))
				.add(((JSONComponent) new JSONComponent("click here please.")
						.setColor(JSONColor.BLUE)
						.setItalic(true)
						.setUnderlined(true))
						.setHoverAction(new JSONHoverAction.ShowText(new JSON(new JSONComponent("Click right there!")
								.setColor(JSONColor.GRAY))))
						.setClickAction(new JSONClickAction.OpenURL("http://google.com")));
		
		JSON json1 = ((JSONComponent) new JSONComponent("And by the way, ").setColor(JSONColor.GRAY)).setHoverAction(new JSONHoverAction.ShowText((new JSONComponent("hi there!").setColor(JSONColor.RED)).toJSON())).combine(
				new JSONComponent("I love this ").setColor(JSONColor.DARK_PURPLE).setBold(true),
				((JSONComponent) new JSONComponent("xxx ").setObfuscated(true).setColor(JSONColor.DARK_BLUE)).setClickAction(new JSONClickAction.RunCommand("hallo Leute, wie geht es euch?!")),
				new JSONComponent("API").setColor(JSONColor.BLUE).setItalic(true).setStrikethrough(true)
				);
		
		JSON json2 = new JSON(new JSONComponent("Oh, and ").setColor(JSONColor.AQUA),
				new JSONComponent("this ").setBold(true),
				new JSONComponent("is also a cool ").setColor(JSONColor.GOLD),
				new JSONComponent("text").setColor(JSONColor.DARK_GREEN).setBold(true)).combine(new JSON(new JSONComponent(", as you can ").setColor(JSONColor.DARK_BLUE)),
						new JSONComponent("see").setItalic(true).setColor(JSONColor.DARK_RED).toJSON());
		
		JSON json3 = ((JSONComponent) new JSONComponent("Hover over here!").setColor(JSONColor.LIGHT_PURPLE)).setHoverAction(new JSONHoverAction.ShowItem(Material.ANVIL)).toJSON();
		
		JSON json4 = new JSON(new JSONComponent("This is a title!").setColor(JSONColor.DARK_RED));
		
		JSON json5 = new JSON(new JSONComponent("Page 1 Line 1\n ").setHoverAction(new JSONHoverAction.ShowItem(Material.STONE)),
				new JSONComponentSimple("Line 2\n"));
		
		for (Player p : plugin.getServer().getOnlinePlayers())
		{
			
			json.send(p);
			JSON.sendJSON(p, json1);
			JSON.sendJSON(p, json2.get());
			json3.send(p);
			json4.sendTitle(p, 10, 60, 10);
			JSON.sendSubtitle(p, json4);
			p.getInventory().addItem(JSON.generateBook(ChatColor.RED + "Book Title", ChatColor.BLUE + "Book Author", json5, json1, json2));  // <- still being tested!
			
		}
		
	}
	
}

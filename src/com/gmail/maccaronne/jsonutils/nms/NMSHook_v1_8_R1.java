//package com.gmail.maccaronne.jsonutils.nms;
//
//import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
//import org.bukkit.entity.Player;
//
//import com.coloredcarrot.mcapi.json.JSON;
//
//import net.minecraft.server.v1_8_R1.ChatSerializer;
//import net.minecraft.server.v1_8_R1.EnumTitleAction;
//import net.minecraft.server.v1_8_R1.PacketPlayOutChat;
//import net.minecraft.server.v1_8_R1.PacketPlayOutTitle;
//import net.minecraft.server.v1_8_R1.PlayerConnection;
//
///**
// * All rights reserved.
// * @author ColoredCarrot
// */
//public class NMSHook_v1_8_R1
//implements NMSHook
//{
//
//	@Override
//	public void sendJSON(String json, Player player)
//	{
//		((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(ChatSerializer.a(json)));
//	}
//
//	@Override
//	public void sendActionBar(String json, Player player)
//	{
//		((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(ChatSerializer.a(json), (byte) 2));
//	}
//
//	@Override
//	public void sendTitle(Player player, JSON title, JSON subtitle, Integer fadeIn, Integer stay, Integer fadeOut)
//	{
//
//		if (player == null)
//			throw new NullPointerException("player cannot be null!");
//
//		CraftPlayer craftplayer = (CraftPlayer) player;
//		PlayerConnection connection = craftplayer.getHandle().playerConnection;
//
//		PacketPlayOutTitle titlePacket = null;
//		PacketPlayOutTitle subtitlePacket = null;
//
//		if (title != null)
//			if (fadeIn == null || stay == null || fadeOut == null)
//				throw new IllegalArgumentException("If title is not null, fadeIn, stay and fadeOut must also not be null!");
//			else
//				titlePacket = new PacketPlayOutTitle(EnumTitleAction.TITLE, ChatSerializer.a(title.get()), fadeIn, stay, fadeOut);
//
//		if (subtitle != null)
//			subtitlePacket = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, ChatSerializer.a(subtitle.get()));
//
//		if (titlePacket != null)
//			connection.sendPacket(titlePacket);
//
//		if (subtitlePacket != null)
//			connection.sendPacket(subtitlePacket);
//
//	}
//
//	@Override
//	public void sendTitle(Player player, JSON json, int fadeIn, int stay, int fadeOut)
//	{
//
//		if (player == null)
//			throw new NullPointerException("player cannot be null!");
//
//		if (json == null)
//			throw new NullPointerException("json cannot be null!");
//
//		CraftPlayer craftplayer = (CraftPlayer) player;
//		PlayerConnection connection = craftplayer.getHandle().playerConnection;
//
//		connection.sendPacket(new PacketPlayOutTitle(EnumTitleAction.TITLE, ChatSerializer.a(json.get()), fadeIn, stay, fadeOut));
//
//	}
//
//	@Override
//	public void sendSubtitle(Player player, JSON json)
//	{
//
//		if (player == null)
//			throw new NullPointerException("player cannot be null!");
//
//		if (json == null)
//			throw new NullPointerException("json cannot be null!");
//
//		CraftPlayer craftplayer = (CraftPlayer) player;
//		PlayerConnection connection = craftplayer.getHandle().playerConnection;
//
//		connection.sendPacket(new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, ChatSerializer.a(json.get())));
//
//	}
//
//}

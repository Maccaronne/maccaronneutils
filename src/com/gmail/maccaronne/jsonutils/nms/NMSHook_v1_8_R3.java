package com.gmail.maccaronne.jsonutils.nms;

import com.gmail.maccaronne.jsonutils.JSON;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_8_R3.PlayerConnection;

/**
 * All rights reserved.
 * @author ColoredCarrot
 */
public class NMSHook_v1_8_R3
implements NMSHook
{

	@Override
	public void sendJSON(String json, Player player)
	{
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(ChatSerializer.a(json)));
	}

	@Override
	public void sendActionBar(String json, Player player)
	{
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(ChatSerializer.a(json), (byte) 2));
	}

	@Override
	public void sendTitle(Player player, JSON title, JSON subtitle, Integer fadeIn, Integer stay, Integer fadeOut)
	{

		if (player == null)
			throw new NullPointerException("player cannot be null!");

		CraftPlayer craftplayer = (CraftPlayer) player;
		PlayerConnection connection = craftplayer.getHandle().playerConnection;

		PacketPlayOutTitle titlePacket = null;
		PacketPlayOutTitle subtitlePacket = null;

		if (title != null)
			if (fadeIn == null || stay == null || fadeOut == null)
				throw new IllegalArgumentException("If title is not null, fadeIn, stay and fadeOut must also not be null!");
			else
				titlePacket = new PacketPlayOutTitle(EnumTitleAction.TITLE, ChatSerializer.a(title.get()), fadeIn, stay, fadeOut);

		if (subtitle != null)
			subtitlePacket = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, ChatSerializer.a(subtitle.get()));

		if (titlePacket != null)
			connection.sendPacket(titlePacket);

		if (subtitlePacket != null)
			connection.sendPacket(subtitlePacket);

	}

	@Override
	public void sendTitle(Player player, JSON json, int fadeIn, int stay, int fadeOut)
	{

		if (player == null)
			throw new NullPointerException("player cannot be null!");

		if (json == null)
			throw new NullPointerException("json cannot be null!");

		CraftPlayer craftplayer = (CraftPlayer) player;
		PlayerConnection connection = craftplayer.getHandle().playerConnection;

		connection.sendPacket(new PacketPlayOutTitle(EnumTitleAction.TITLE, ChatSerializer.a(json.get()), fadeIn, stay, fadeOut));

	}

	@Override
	public void sendSubtitle(Player player, JSON json)
	{

		if (player == null)
			throw new NullPointerException("player cannot be null!");

		if (json == null)
			throw new NullPointerException("json cannot be null!");

		CraftPlayer craftplayer = (CraftPlayer) player;
		PlayerConnection connection = craftplayer.getHandle().playerConnection;

		connection.sendPacket(new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, ChatSerializer.a(json.get())));

	}

}

package com.gmail.maccaronne.jsonutils.nms;

import com.gmail.maccaronne.jsonutils.JSON;
import org.bukkit.entity.Player;


/**
 * All rights reserved.
 * @author ColoredCarrot
 */
public interface NMSHook
{

	/**
	 * Sends a JSON message to a player.
	 * @param json (String) - the plain JSON
	 * @param player (Player) - the player
	 */
	void sendJSON(String json, Player player);
	
	/**
	 * Sends an actionbar to a player.
	 * @param json (String) - the plain JSON
	 * @param player (Player) - the player
	 */
	void sendActionBar(String json, Player player);
	
	/**
	 * Sends a title and/or subtitle to a specified player.
	 * @param player (Player) - the player receiving the title/subtitle
	 * @param title (JSON) - the title, may be null to send no title
	 * @param subtitle (JSON) - the subtitle, may be null to send no subtitle
	 * @param fadeIn (Integer) - over how much time the title should fade in, may be null if title is null
	 * @param stay (Integer) - how much time the title should stay, may be null if title is null
	 * @param fadeOut (Integer) - over how much time the title should fade out, may be null if title is null
	 */
	void sendTitle(Player player, JSON title, JSON subtitle, Integer fadeIn, Integer stay, Integer fadeOut);
	
	/**
	 * Sends a title to a player.
	 * @param player (Player) - the player receiving the title
	 * @param json (JSON) - the title
	 * @param fadeIn (int) - over how much time the title should fade in
	 * @param stay (int) - how much time the title should stay
	 * @param fadeOut (int) - over how much time the title should fade out
	 */
	void sendTitle(Player player, JSON json, int fadeIn, int stay, int fadeOut);
	
	/**
	 * Sends a subtitle to a player.
	 * @param player (Player) - the player receiving the title
	 * @param json (JSON) - the title
	 * @param fadeIn (int) - over how much time the title should fade in
	 * @param stay (int) - how much time the title should stay
	 * @param fadeOut (int) - over how much time the title should fade out
	 */
	void sendSubtitle(Player player, JSON json);
	
}

package com.gmail.maccaronne;

import com.gmail.maccaronne.aaccompat.AACCompater;
import com.gmail.maccaronne.aaccompat.MagicSpellCompat;
import com.gmail.maccaronne.aaccompat.MythicMobsCompat;
import com.gmail.maccaronne.nmsUtils.NmsUtils;
import com.gmail.maccaronne.nmsUtils.NmsUtils_1_10_R1;
import com.gmail.maccaronne.nmsUtils.NmsUtils_1_8_R3;
import com.gmail.maccaronne.nmsUtils.NmsUtils_1_9_R2;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.NumberConversions;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Maccaronne on 2015/10/18.
 * Vos autem Dominus benedic hoc signum
 */
public class MaccaronneUtils extends JavaPlugin implements CommandExecutor {

    private static MaccaronneUtils instance;
    public static boolean isAACEnabled;
    public static NmsUtils utils;
    public static int version;

    public static MaccaronneUtils getInstance() {
        return instance;
    }

    public static void addValidPlayer(String p) {
        if (isAACEnabled) AACCompater.addValidAttacker(p);
    }

    public static String printStackTraces() {
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
            sb.append(element.toString()).append("\n");
        }
        return sb.toString();
    }

    public void onEnable() {
        instance = this;
        File savedLocation = MaccaronneUtils.getInstance().getDataFolder();
        if (!savedLocation.exists()) savedLocation.mkdirs();
        PluginManager pm = Bukkit.getPluginManager();
        getCommand("maccaronne").setExecutor(new MaccaronneCommand());
        try {
            Class.forName("me.konsolas.aac.api.AACAPI");
            getLogger().log(Level.INFO, "AAC is enabled!!");
            isAACEnabled = true;
            pm.registerEvents(new AACCompater(), this);
        } catch (ClassNotFoundException e) {
            getLogger().log(Level.INFO, "AAC is not detected");
        }
        if (isAACEnabled) {
            getLogger().log(Level.INFO, "Adding AAC Compatabilities...");
            try {
                Class.forName("net.elseland.xikage.MythicMobs.API.IMobsAPI");
                getLogger().log(Level.INFO, "MythicMobs is enabled!!");
                pm.registerEvents(new MythicMobsCompat(), this);
            } catch (ClassNotFoundException e) {
                getLogger().log(Level.INFO, "MythicMobs is not detected");
            }
            try {
                Class.forName("com.nisovin.magicspells.MagicSpells");
                getLogger().log(Level.INFO, "MagicSpells is enabled!!");
                pm.registerEvents(new MagicSpellCompat(), this);
            } catch (ClassNotFoundException e) {
                getLogger().log(Level.INFO, "MagicSpells is not detected");
            }
        }
        String nmsver = Bukkit.getServer().getClass().getPackage().getName();
        nmsver = nmsver.substring(nmsver.lastIndexOf(".") + 1);

        try {
            Class.forName("net.minecraft.server.v1_8_R3.MinecraftServer");
            utils = new NmsUtils_1_8_R3();
            version = 188;
            System.out.println("Server version: 1.8.8");
        } catch (ClassNotFoundException e) {
            try {
                Class.forName("net.minecraft.server.v1_9_R2.MinecraftServer");
                version = 194;
                utils = new NmsUtils_1_9_R2();
                System.out.println("Server version: 1.9.4");
            } catch (ClassNotFoundException e1) {
                try {
                    Class.forName("net.minecraft.server.v1_10_R1.MinecraftServer");
                    version = 1101;
                    utils = new NmsUtils_1_10_R1();
                    System.out.println("Server version: 1.10.1");
                } catch (ClassNotFoundException e2) {
                    System.out.println("Unsupported spigot version! Please use either 1.88 or 1.94!");
                }
            }
        }
    }


    public void reload() {
        reloadConfig();
    }

    public static PlayerDataSection getPlayerData(String player, JavaPlugin plugin) {
        return new PlayerDataSection(player, plugin.getName());
    }

//    private static Map<String, PlayerDataSection> playerDataSectionMap = new HashMap<>();
//
//    public static PlayerDataSection getPlayerData(String player, JavaPlugin plugin) {
//        if (!playerDataSectionMap.keySet().contains(player)) {
//            PlayerDataSection playerDataSection = new PlayerDataSection(player, plugin.getName());
//            playerDataSectionMap.put(player, playerDataSection);
//            return playerDataSection;
//        } else {
//            return playerDataSectionMap.get(player);
//        }
//    }
//
//    @EventHandler
//    public void onPlayerQuit(PlayerQuitEvent e) {
//        if (playerDataSectionMap.containsKey(e.getPlayer().getName())) {
//            playerDataSectionMap.remove(e.getPlayer().getName());
//        }
//    }

    public static String getItemName(ItemStack itemStack) {
        if (itemStack == null || itemStack.getType() == Material.AIR) return null;
        ItemMeta meta = itemStack.getItemMeta();
        if (meta == null) return null;
        String name = meta.getDisplayName();
        return name;
    }


    public static void sendTitle(Player p, String title, String subtitle) {
        sendTitle(p, title, subtitle, 0, 100, 20);
    }

    public static void sendTitle(Player p, String title, String subtitle, int fadeIn, int stay, int fadeOut) {
        Title.sendTitle(p, fadeIn, stay, fadeOut, title, subtitle);
    }

    public static void sendTitle(Player p, String title, String subtitle, int stay) {
        sendTitle(p, title, subtitle, 0, stay, 20);
    }

    public static void executeCommand(String cmd) {
        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), cmd);
    }


    public static void executeCmdsForPlayer(List<String> cmds, String player) {
        for (String cmd : cmds) {
            Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), cmd.replace("%p", player));
        }
    }

    public static String toString(Location location) {
        return location.getWorld().getName() + "," + location.getX() + "," + location.getY() + "," + location.getZ() + "," + location.getYaw() + "," + location.getPitch();
    }

    private final static DecimalFormat FORMAT = new DecimalFormat("0.##");

    public static String format(Number number) {
        return FORMAT.format(number);
    }

    public static String toString(Number[] num) {
        StringBuilder sb = new StringBuilder();
        for (Number number : num) {
            sb.append(number + ",");
        }
        return sb.toString();
    }

    public static Location getLocation(String s) {
        if (s == null) return null;
        String[] locations = s.split(",");
        if (locations.length >= 6) {
            return new Location(Bukkit.getWorld(locations[0]), Double.parseDouble(locations[1]), Double.parseDouble(locations[2]), Double.parseDouble(locations[3]), NumberConversions.toFloat(locations[4]), NumberConversions.toFloat(locations[5]));
        } else {
            return new Location(Bukkit.getWorld(locations[0]), Double.parseDouble(locations[1]), Double.parseDouble(locations[2]), Double.parseDouble(locations[3]));
        }
    }

    public static String buildURLMessage(String text, String command, String hoverMessage) {
        return "[\"\",{\"text\":\"" + text + "\",\"color\":\"dark_green\",\"bold\":true,\"underlined\":true,\"clickEvent\":{\"action\":\"open_url\",\"value\":\"" + command + "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"" + hoverMessage + "\"}]}}}]";
    }

    public static String buildClickableMessage(String text, String command, String hoverMessage) {
        return "[\"\",{\"text\":\"" + text + "\",\"color\":\"dark_green\",\"bold\":true,\"underlined\":true,\"clickEvent\":{\"action\":\"run_command\",\"value\":\"" + command + "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"" + hoverMessage + "\"}]}}}]";
    }

    public static String buildPromptMessage(String text, String command, String hoverMessage) {
        return "[\"\",{\"text\":\"" + text + "\",\"color\":\"dark_green\",\"bold\":true,\"underlined\":true,\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"" + command + "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"" + hoverMessage + "\"}]}}}]";
    }

    public static void sendJsonMessage(Player p, String s) {
        getUtils().sendJsonMessage(p, s);
    }

//    public static void sendActionBar(Player p, String s) {
//        try {
//            Bukkit.getServer().dispatchCommand(
//                    Bukkit.getServer().getConsoleSender(),
//                    "tm amsg " + p.getName() + " " + s
//            );
//        } catch (Exception ex) {
//        }
//    }

    @Deprecated
    public static void sendActionBar(final Player player, final String message, int duration) {
        sendActionBar(player, message);

        if (duration >= 0) {
            // Sends empty message at the end of the duration. Allows messages shorter than 3 seconds, ensures precision.
            new BukkitRunnable() {
                @Override
                public void run() {
                    sendActionBar(player, "");
                }
            }.runTaskLater(getInstance(), duration + 1);
        }

        // Re-sends the messages every 3 seconds so it doesn't go away from the player's screen.
        while (duration > 60) {
            duration -= 60;
            int sched = duration % 60;
            new BukkitRunnable() {
                @Override
                public void run() {
                    sendActionBar(player, message);
                }
            }.runTaskLater(getInstance(), (long) sched);
        }
    }

    public static void sendActionBar(Player player, String message) {
        utils.sendActionBar(player, message);
    }

    public static void removeHeldItemByAmount(Player p, int amount) {
        PlayerInventory inv = p.getInventory();
        ItemStack item = inv.getItemInHand();
        if (item.getAmount() <= amount) {
            item.setType(Material.AIR);
        } else {
            item.setAmount(item.getAmount() - amount);
        }
        p.setItemInHand(item);
    }

    public static ItemStack removeItemByAmount(ItemStack item, int amount) {
        if (item.getAmount() <= amount) {
            item = null;
        } else {
            item.setAmount(item.getAmount() - amount);
        }
        return item;
    }

    public static Location getLocationXBlocksForward(Player p, double maxDistance) {
        if (maxDistance < 1) return p.getEyeLocation();
        BlockIterator itr = new BlockIterator(p, 1);
        Block b = null;
        for (int i = 0; i < Math.ceil(maxDistance); i++) {
            b = itr.next();
            if (i + 1 > Math.ceil(maxDistance) && b.getLocation().distanceSquared(p.getEyeLocation()) < Math.pow(maxDistance, 2)) {
                b = itr.next();
                break;
            }
        }
        return b.getLocation();
    }

    public static NmsUtils getUtils() {
        return utils;
    }

    public static void playEffect(LivingEntity entity, Effect effect, int amount) {
        playEffect(entity, effect, amount, 0.3f);
    }

    public static void playEffect(LivingEntity entity, Effect effect, int amount, float speed) {
        playEffect(entity, effect, amount, speed, 1);
    }

    public static void playEffect(LivingEntity entity, Effect effect, int amount, float speed, int radius) {
        entity.getWorld().spigot().playEffect(entity.getEyeLocation(), effect, 1, 1, 1, 1, 1, speed, amount, radius);
    }

    public static boolean hasItem(Player p, String materialName, int amount) {
        if (materialName == null) return true;
        int hasAmount = 0;
        for (ItemStack itemStack : p.getInventory()) {
            if (materialName.equals(MaccaronneUtils.getItemName(itemStack))) {
                hasAmount += itemStack.getAmount();
                if (hasAmount >= amount) break;
            }
        }
        return hasAmount >= amount;
    }

    public static boolean hasItem(Player p, ItemStack material) {
        String materialName = MaccaronneUtils.getItemName(material);
        int hasAmount = 0;
        for (ItemStack itemStack : p.getInventory()) {
            if ((materialName == null && material.equals(itemStack)) || (materialName != null && materialName.equals(MaccaronneUtils.getItemName(itemStack)))) {
                hasAmount += itemStack.getAmount();
                if (hasAmount >= material.getAmount()) break;
            }
        }
        return hasAmount >= material.getAmount();
    }

}

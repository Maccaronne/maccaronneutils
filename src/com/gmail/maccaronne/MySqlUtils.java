//package com.gmail.maccaronne;
//
//import com.mysql.jdbc.Connection;
//import org.bukkit.ChatColor;
//import org.bukkit.command.CommandSender;
//
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by Maccaronne on 2016/3/8.
// * Vos autem Dominus benedic hoc signum
// */
//public class MySqlUtils {
//    private static Connection connection;
//    private static String database, username, password;
//
//    public static void bootSqlConnection() {
//        database = MaccaronneUtils.getInstance().getConfig().getString("Mysql.database");
//        username = MaccaronneUtils.getInstance().getConfig().getString("Mysql.username");
//        password = MaccaronneUtils.getInstance().getConfig().getString("Mysql.password");
//    }
//
//    // synchronized: 这个方法同时只会存在一个，若有多个则会等待上一个完成
//    public static synchronized boolean openConnection() {
//        try {
//            connection = (Connection) DriverManager.getConnection(database, username, password);
//            return true;
//        } catch (SQLException e) {
//            return false;
//        }
//    }
//
//    // synchronized: 这个方法同时只会存在一个，若有多个则会等待上一个完成
//    public static synchronized void closeConnection() {
//        try {
//            connection.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static synchronized void sendTopMessages(CommandSender s, int page) {
//        if (openConnection()) {
//            try {
//                PreparedStatement sql = connection.prepareStatement("SELECT Player,Power FROM LegendaryPowers ORDER BY Power DESC;");
//
//                ResultSet result = sql.executeQuery();
//                int start = ((page - 1) * 10) + 1;
//                if (result.absolute(start)) {
//                    s.sendMessage(LegendaryPowers.getInstance().getConfig().getString("TopTitle"));
//                    for (int c = 1; c != 10; c++) {
//                        s.sendMessage("§e  " + ((page - 1) * 10 + c) + ".   " + result.getString("Player") + "   §d-   §6" + result.getInt("Power"));
//                        if (!result.next()) {
//                            break;
//                        }
//                    }
//                } else {
//                    s.sendMessage(ChatColor.RED + "并没有那么多页！");
//                }
//                sql.close();
//                result.close();
//            } catch (Exception e1) {
//                e1.printStackTrace();
//            } finally {
//                closeConnection();
//            }
//        }
//    }
//
//    public static int getRanking(String playerName) {
//        if (openConnection()) {
//            try {
//                PreparedStatement sql = connection.prepareStatement("SELECT Player,Power FROM LegendaryPowers ORDER BY Power DESC LIMIT 0,100;");
//                ResultSet result = sql.executeQuery();
//                while (result.next()) {
//                    if (result.getString("Player").equals(playerName)) {
//                        int row = result.getRow();
//                        sql.close();
//                        result.close();
//                        return row;
//                    }
//                }
//            } catch (Exception e1) {
//                e1.printStackTrace();
//            } finally {
//                closeConnection();
//            }
//        }
//        return 0;
//    }
//
//
//    private synchronized static boolean playerDataContainPlayer(String playerName) {
//        try {
//            // 新建一个PreparedStatement对象，这个对象用于存储sql语句，调用它可以执行该语句
//            PreparedStatement sql = connection.prepareStatement("SELECT * FROM LegendaryPowers WHERE player=?;");
//            sql.setString(1, playerName);
//            ResultSet resultSet = sql.executeQuery();
//            boolean containsPlayer = resultSet.next();
//
//            sql.close();
//            resultSet.close();
//
//            return containsPlayer;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//    private static class SqlTableStructure {
//
//        String tableName;
//
//        public SqlTableStructure(String tableName) {
//            this.tableName = tableName;
//        }
//
//        List<String> columnNames = new ArrayList<>();
//        List<ColumnType> columnTypes = new ArrayList<>();
//
//        private enum ColumnType {
//            INT,
//            VARCHAR;
//        }
//
//        public void addColumn(String columnName, ColumnType type) {
//            this.columnNames.add(columnName);
//            this.columnTypes.add(type);
//        }
//
//        @Override
//        public String toString() {
//            StringBuilder sb = new StringBuilder('(');
//            for (int i = 0; i < columnTypes.size(); i++) {
//                sb.append(columnNames.get(i) + " " + columnTypes.get(i).name());
//                if (i < columnTypes.size() - 1) sb.append(',');
//            }
//            return sb.toString();
//        }
//
//        public static synchronized void createTable(String tableName, SqlTableStructure sqlTable) {
//            if (openConnection()) {
//                try {
//                    PreparedStatement createTable = connection.prepareStatement(
//                            "CREATE TABLE if not exists " + tableName + " " + sqlTable.toString() + ";");
//                    createTable.executeUpdate();
//
//                    createTable.close();
//                } catch (Exception e1) {
//                    e1.printStackTrace();
//                } finally {
//                    closeConnection();
//                }
//            }
//        }
//
//        public static synchronized void writeData(String playerName, int power) {
//            if (openConnection()) {
//                try {
//                    if (playerDataContainPlayer(playerName)) {
//
//                        PreparedStatement loginsUpdate = connection.prepareStatement("UPDATE `LegendaryPowers` SET Power=? WHERE Player=?;");
//                        loginsUpdate.setInt(1, power);
//                        loginsUpdate.setString(2, playerName);
//                        loginsUpdate.executeUpdate();
//
//                        loginsUpdate.close();
//                    } else {
//                        PreparedStatement newPlayer = connection.prepareStatement("INSERT INTO LegendaryPowers (Player, Power) values(?,?);");
//                        newPlayer.setString(1, playerName);
//                        newPlayer.setInt(2, power);
//                        newPlayer.execute();
//                        newPlayer.close();
//                    }
//                } catch (Exception e1) {
//                    e1.printStackTrace();
//                } finally {
//                    closeConnection();
//                }
//            }
//        }
//    }
//}

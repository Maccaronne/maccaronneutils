package com.gmail.maccaronne.nmsUtils;

import net.minecraft.server.v1_8_R3.DamageSource;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

/**
 * Created by 微笑笑笑 on 2016/6/15.
 */
public class NmsUtils_1_8_R3 implements NmsUtils {

    public void sendJsonMessage(Player p, String s) {
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(s)));
    }


    public void sendActionBar(Player player, String message) {
        CraftPlayer p = (CraftPlayer) player;
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message.replaceAll("&", "§") + "\"}");
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
        p.getHandle().playerConnection.sendPacket(ppoc);
    }

    @Override
    public ItemStack getOffHandItem(PlayerInventory inv) {
        return null;
    }

    @Override
    public void openBook(Player p, ItemStack book) {
        ((CraftPlayer) p).getHandle().openBook(CraftItemStack.asNMSCopy(book));
    }

    @Override
    public void sendItemCooldownOverlay(Player p, ItemStack item, int cooldown) {
    }

    @Override
    public void setKiller(Player attacker, LivingEntity victim) {
        ((CraftLivingEntity) victim).getHandle().killer = ((CraftPlayer) attacker).getHandle();
    }

    @Override
    public void killEntity(Player attacker, LivingEntity entity) {
        ((CraftLivingEntity) entity).getHandle().die(DamageSource.playerAttack(((CraftPlayer) attacker).getHandle()));
    }
}

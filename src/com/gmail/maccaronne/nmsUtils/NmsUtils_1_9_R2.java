package com.gmail.maccaronne.nmsUtils;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.gmail.maccaronne.utils.ReflectionUtils;
import net.minecraft.server.v1_9_R2.*;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_9_R2.inventory.CraftInventoryPlayer;
import org.bukkit.craftbukkit.v1_9_R2.inventory.CraftItemStack;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by ΢ЦЦЦ on 2016/6/15.
 */
public class NmsUtils_1_9_R2 implements NmsUtils {

    public void sendJsonMessage(Player p, String s) {
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(s)));
    }

    public void sendActionBar(Player player, String message) {
        CraftPlayer p = (CraftPlayer) player;
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message.replaceAll("&", "��") + "\"}");
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
        p.getHandle().playerConnection.sendPacket(ppoc);
    }

    @Override
    public ItemStack getOffHandItem(PlayerInventory inv) {
        return ((CraftInventoryPlayer) inv).getItemInOffHand();
    }

    @Override
    public void openBook(Player p, ItemStack book) {
        try {
            Class<?> enumHand = ReflectionUtils.PackageType.MINECRAFT_SERVER.getClass("EnumHand");
            EnumHand[] enumArray = (EnumHand[]) enumHand.getEnumConstants();
            ((CraftPlayer) p).getHandle().a(CraftItemStack.asNMSCopy(book), enumArray[0]);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    static ProtocolManager manager;

    @Override
    public void sendItemCooldownOverlay(Player p, ItemStack item, int cooldown) {
        if (item == null || item.getType() == Material.AIR) return;
        PacketContainer packet = getProtocolManager().createPacket(PacketType.Play.Server.SET_COOLDOWN);
        packet.getIntegers().write(0, cooldown);
        packet.getModifier().write(0, CraftItemStack.asNMSCopy(item).getItem());
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(p, packet);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void killEntity(Player attacker, LivingEntity entity) {
        ((CraftLivingEntity) entity).getHandle().die(DamageSource.playerAttack(((CraftPlayer) attacker).getHandle()));
    }

    @Override
    public void setKiller(Player attacker, LivingEntity victim) {
        ((CraftLivingEntity) victim).getHandle().killer = ((CraftPlayer) attacker).getHandle();
    }


    private ProtocolManager getProtocolManager() {
        if (manager == null) manager = ProtocolLibrary.getProtocolManager();
        return manager;
    }
}

package com.gmail.maccaronne.nmsUtils;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

/**
 * Created by ΢ЦЦЦ on 2016/6/15.
 */
public interface NmsUtils {

    void sendJsonMessage(Player p, String s);

    void sendActionBar(Player player, String message);

    ItemStack getOffHandItem(PlayerInventory inv);

    void openBook(Player p, ItemStack book);

    void sendItemCooldownOverlay(Player p, ItemStack item, int cooldown);

    void killEntity(Player attacker, LivingEntity entity);

    void setKiller(Player attacker, LivingEntity victim);
}

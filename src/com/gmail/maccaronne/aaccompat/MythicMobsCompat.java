package com.gmail.maccaronne.aaccompat;

import com.gmail.maccaronne.MaccaronneUtils;
import net.elseland.xikage.MythicMobs.API.Bukkit.Events.MythicMobSkillEvent;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by 微笑笑笑 on 2016/4/18.
 */
public class MythicMobsCompat implements Listener {

    @EventHandler
    public void onMobExecuteSkill(MythicMobSkillEvent e) {
        LivingEntity target = e.getTarget();
        if (e.getSkillName().equals("throw")) {
            if (target != null && target instanceof Player) {
                MaccaronneUtils.addValidPlayer(target.getName());
            }
        }
    }
}

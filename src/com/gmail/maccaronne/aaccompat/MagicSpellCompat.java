package com.gmail.maccaronne.aaccompat;

import com.gmail.maccaronne.MaccaronneUtils;
import com.nisovin.magicspells.Spell;
import com.nisovin.magicspells.events.SpellCastEvent;
import com.nisovin.magicspells.spells.instant.LeapSpell;
import com.nisovin.magicspells.spells.targeted.ForcetossSpell;
import com.nisovin.magicspells.spells.targeted.GeyserSpell;
import com.nisovin.magicspells.spells.targeted.PainSpell;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by 微笑笑笑 on 2016/4/18.
 */
public class MagicSpellCompat implements Listener {

    @EventHandler
    public void onSpellCast(SpellCastEvent e) {
        Spell spell = e.getSpell();
        if (spell instanceof PainSpell || spell instanceof ForcetossSpell || spell instanceof GeyserSpell|| spell instanceof LeapSpell) {
            MaccaronneUtils.addValidPlayer(e.getCaster().getName());
        }
    }
}

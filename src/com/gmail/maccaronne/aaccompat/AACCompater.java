package com.gmail.maccaronne.aaccompat;

import com.gmail.maccaronne.MaccaronneUtils;
import me.konsolas.aac.api.AACAPI;
import me.konsolas.aac.api.AACAPIProvider;
import me.konsolas.aac.api.HackType;
import me.konsolas.aac.api.PlayerViolationEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by 微笑笑笑 on 2016/4/10.
 */
public class AACCompater implements Listener {

    private static Map<String, Integer> validAttackers;
    AACAPI api;

    public AACCompater() {
        validAttackers = new HashMap<>();
        Bukkit.getScheduler().runTaskTimerAsynchronously(MaccaronneUtils.getInstance(), () -> {
            Iterator<String> iterator = validAttackers.keySet().iterator();
            while (iterator.hasNext()) {
                String name = iterator.next();
                Integer i = validAttackers.get(name) - 5;
                if (i <= 0) {
                    iterator.remove();
                } else {
                    validAttackers.put(name, i);
                }
            }
        }, 5, 5);
    }

    public static void addValidAttacker(String playerName) {
        validAttackers.put(playerName, 20);
    }

    public boolean isValidAttacker(String name) {
        return validAttackers.keySet().contains(name);
    }

    @EventHandler
    public void onPlayerViolateAttackCheck(PlayerViolationEvent e) {
        if (e.getHackType() == HackType.COMBATIMPOSSIBLE || e.getHackType() == HackType.NORMALMOVEMENTS || e.getHackType() == HackType.SPEED || e.getHackType() == HackType.ANGLE || e.getHackType() == HackType.REACH || e.getHackType() == HackType.KILLAURA || e.getHackType() == HackType.FIGHTSPEED || e.getHackType() == HackType.FORCEFIELD || e.getHackType() == HackType.COMBATIMPOSSIBLE) {
            if (isValidAttacker(e.getPlayer().getName())) {
                e.setCancelled(true);
                if (api == null) {
                    api = AACAPIProvider.getAPI();
                }
                api.setViolationLevel(e.getPlayer(), e.getHackType(), 0);
            }
        }
    }


}

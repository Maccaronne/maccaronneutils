package com.gmail.maccaronne.utils;

import com.gmail.maccaronne.MaccaronneUtils;
import com.mysql.jdbc.Connection;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by 微笑笑笑 on 2016/10/6.
 */
public class SharedFileUtils {
    protected static Connection connection;
    private static String database, username, password;

    public static void bootSqlConnection() {
        try {
            YamlConfiguration database = new YamlConfiguration();
            database.load(MaccaronneUtils.getInstance().getResource("Database.yml"));
            SharedFileUtils.database = database.getString("Mysql.database");
            username = database.getString("Mysql.username");
            password = database.getString("Mysql.password");
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }


    public static synchronized boolean openConnection() {
        if (database == null) bootSqlConnection();
        try {
            if (connection == null || connection.isClosed()) {
                connection = (Connection) DriverManager.getConnection(database, username, password);
                return true;
            }
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public static synchronized void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static YamlConfiguration getFile(String directory) {
        if (openConnection()) {
            try {
                if (playerDataContainplayer(directory)) {
                    PreparedStatement sql = connection.prepareStatement("SELECT content FROM files WHERE directory=?;");
                    sql.setString(1, directory);

                    ResultSet result = sql.executeQuery();
                    if (result.next()) {
                        String rank = result.getString("content");
                        sql.close();
                        result.close();
                        YamlConfiguration yamlConfiguration = new YamlConfiguration();
                        yamlConfiguration.loadFromString(rank);
                        return yamlConfiguration;
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                closeConnection();
            }
        }
        Bukkit.getLogger().warning("§c无法获取文件: " + directory + "! 请确保BC端的BungeeFileHost文件夹中存在此文件！");
        return null;
    }

    public synchronized static boolean playerDataContainplayer(String directory) {
        try {
            PreparedStatement sql = connection.prepareStatement("SELECT * FROM files WHERE directory=?;");
            sql.setString(1, directory);
            ResultSet resultSet = sql.executeQuery();
            boolean containsplayer = resultSet.next();

            sql.close();
            resultSet.close();

            return containsplayer;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}